PART 1:
Question 1:
Write a regex to extract all the numbers with orange color background from the below text in italics.

{"orders":[{"id":1},{"id":2},{"id":3},{"id":4},{"id":5},{"id":6},{"id":7},{"id":8},{"id":9},{"id":10},{"id":11},{"id":648},{"id":649},{"id":650},{"id":651},{"id":652},{"id":653}],"errors":[{"code":3,"message":"[PHP Warning #2] count(): Parameter must be an array or an object that implements Countable (153)"}]}

ANSWER: 
Regular expression can be used to extract all the integers used in the dictionary.




Question 2:
Here’s the list of reviews of Chrome apps - scraped from Playstore.  DataSet Link
Problem statement - There are times when a user writes Good, Nice App or any other positive text, in the review and gives 1-star rating. Your goal is to identify the reviews where the semantics of review text does not match rating. 
Your goal is to identify such ratings where review text is good, but rating is negative- so that the support team can point this to users. 
Deploy it using - Flask/Streamlit etc and share the live link. 
Important
In this data app - the user will upload a csv and you would be required to display the reviews where the content doesn’t match ratings.  This csv will be in the same format as the DataSet Link
Bonus Points - If you deploy the app with Authentication. 

ANSWER:
1. The attributes "text" and "stars" are sufficient to analyse the required problem statement. Hence, remove the remaining columns.
2. After this, the records where star is 1 or 2 are filtered to get all the negative reviews. Then apply NLTK sentiment analyzer.




Question 3
Ranking Data - Understanding the co-relation between keyword rankings with description or any other attribute. Here’s the dataset. 
Suggested questions:
1.	Is there any co-relation between short description, long description and ranking? Does the placement of keyword (for example - using a keyword in the first 10 words - have any co-relation with the ranking)?
2.	Does APP ID (Also known as package name) play any role in ranking?  
3.	Any other pattern or good questions that you can think of and answer?


ANSWER:
Chi square test is used to find out the correlation between two variables, the hypothesis of chi squared test is

H0: There is no relation between the variables
H1: There is relation between the variables

If the P-value is higher than 0.05, H0 will be accepted otherwise rejected.

1.	Is there any co-relation between short description, long description and ranking? Does the placement of keyword (for example - using a keyword in the first 10 words - have any co-relation with the ranking)?
Since p value is 0.0, there is a good co-relation between short description, long description and ranking
2.	Does APP ID (Also known as package name) play any role in ranking?  
App ID is correlated with Ranking and have good coefficient with Rank. Hence, App ID plays important role in Rank



PART 2:
QUESTION 1:
Check if the sentence is Grammatically correct: Please use any pre-trained model or use text from open datasets. Once done, please evaluate the English Grammar in the text column of the below dataset. 
DataSet Link
Optional - if you can indicate the grammatical accuracy of sentences in percentage or on number scale (1-10), that would be an added plus - but is not essential. 

ANSWER:
I have used Google's T5 model. T5 is a text-to-text model and it produces a standalone piece of text. Using the similarity score we will decide whether the given text is grammatically correct or not.



More Bonus points (You can write answers to these in ReadMe)
1.	Write about any difficult problem that you solved. (According to us difficult - is something which 90% of people would have only 10% probability in getting a similarly good solution). 

ANSWER: 
Recently We have received an assessment from our faculty and we all found it very difficult to code it. Anyway I have browsed all the sample datasets and plotted so many graphs to find the relationship between the variables and submitted the assignment. Since I do not have any previous experience it was quite difficult to understand the problem statement and to conclude with a clear objective.  


2.	Formally, a vector space V' is a subspace of a vector space V if
○	V' is a vector space
○	every element of V′ is also an element of V.
Note that ordered pairs of real numbers (a,b) a,b∈R form a vector space V. Which of the following is a subspace of V?
●	The set of pairs (a, a + 1) for all real a
●	The set of pairs (a, b) for all real a ≥ b
●	The set of pairs (a, 2a) for all real a
●	The set of pairs (a, b) for all non-negative real a,b

ANSWER:
The set of pairs (a, b) for all non-negative real a,b
